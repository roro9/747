import React from 'react';
import MapPanel from './Panels/MapPanel/MapPanel'
import NonMapPanel from './Panels/NonMapPanel/NonMapPanel'
import './App.css'; // without this it doesn't watch for changes when css changed
import firebase from 'firebase/app';
import 'firebase/database';
import { DB_CONFIG } from './config'

const initCenter = {
  "data" : {
    "coord" : {
      "lat" : 28.524811,
      "lng" : 77.206621
    },
    "hasBiology" : true,
    "hasChemistry" : true,
    "hasEleven" : true,
    "hasMath" : true,
    "hasNine" : true,
    "hasPhysics" : true,
    "hasTen" : true,
    "hasTwelve" : true,
    "locality" : "Location1",
    "zone" : "South Delhi",
    "address" : "testing address 1,23 60610",
    "website" : "www.mywebsite.com",
    "phone" : ["7738843331", "9873715951"],
    "timings" : {
      "Monday": {
        "open": "10am",
        "close": "4pm"
      },
      "Tuesday": {
        "open": "10am",
        "close": "4pm"
      },
      "Wednesday": {
        "open": "10am",
        "close": "4pm"
      },
      "Thursday": {
        "open": "10am",
        "close": "4pm"
      },
      "Friday": {
        "open": "10am",
        "close": "4pm"
      },
      "Saturday": {
        "open": "10am",
        "close": "4pm"
      },
      "Sunday": {
        "open": "10am",
        "close": "4pm"
      } 
    }
  },
  name : "..."
};


class AllPanels extends React.Component {
  render() {
    console.log('AllPanels');
    console.log(this.props);

    return (
      <div className="App-AllPanels">
        <NonMapPanel centers={this.props.centers} 
                handleListingClick={this.props.handleListingClick}
                handleFilterClick={this.props.handleFilterClick}
                onResetFilters={this.props.onResetFilters}
                filters={this.props.filters}
                selectedCenter={this.props.selectedCenter}
        />

        <MapPanel   handleMapDetailBtnClick={this.props.handleMapDetailBtnClick} 
                    isMapView={this.props.isMapView}
                    selectedCenter={this.props.selectedCenter}
                    centers={this.props.centers}
                    onMarkerClick={this.props.onMarkerClick}
        />
      </div>
    );
  }
}

class Header extends React.Component {
  render() {
    return (
      <div className="App-Header">
        <div className="LeftMajor">
          <div className="Header-Logo">
            MapMyClasses
          </div>
        </div>
        
        <div className="RightMinor">
          <div>
            Get Listed
          </div>
        </div>
        
      </div>
    );
  }
}

class App extends React.Component { 

  constructor(props) {
    super(props);
    
    this.state = {
      filters: {
        classes: {
          hasNine: false,
          hasTen: false,
          hasEleven: false,
          hasTwelve: false
        },
        subjects: {
          hasMath: false,
          hasPhysics: false,
          hasChemistry: false,
          hasBiology: false
        }
      },
      centers: [], // this is what is shown
      allCenters: [], // this is total list
      isMapView: true,
      selectedCenter: initCenter
    };

    this.app = firebase.initializeApp(DB_CONFIG);
    this.database = this.app.database().ref().child('s747'); // arg of child() is the parent node which will return the data from firebase
  }

  colorOption = (isFilterOn, btnElem) => {
    isFilterOn ? btnElem.classList.add('optionSelect') : btnElem.classList.remove('optionSelect');
  }

  onResetFilters = () => {
    console.log('filters reset');
    
    // 1. reset filters state
    // 2. clear colored attribs
    // 3. set centers to all

    this.setState({
      filters: {
        classes: {
          hasNine: false,
          hasTen: false,
          hasEleven: false,
          hasTwelve: false
        },
        subjects: {
          hasMath: false,
          hasPhysics: false,
          hasChemistry: false,
          hasBiology: false
        }
      },
      centers: this.state.allCenters
    }, () => {
      console.log("filterest have been reset...only coloring left");

      const options = document.getElementsByClassName('option');
      for(let i=0; i<options.length; i++) {
        options[i].classList.remove('optionSelect');
      }

      console.log(this.state);
    })
  }

  handleFilterClick = (type, value) => {
    console.log('filter clicked: ' + type + ': ' + value);
    
    // update color of btn also
    const btnClass = "option-" + type + "-" + value;
    const btnElem = document.getElementsByClassName(btnClass)[0];

    const localFilters = this.state.filters;
    
    switch(type) {

      case 'Class':
        switch(value) {
          case 'IX':
            localFilters.classes.hasNine = !localFilters.classes.hasNine;
            this.colorOption(localFilters.classes.hasNine, btnElem);
            break;
          case 'X':
            localFilters.classes.hasTen = !localFilters.classes.hasTen;
            this.colorOption(localFilters.classes.hasTen, btnElem);
            break; 
          case 'XI':
            localFilters.classes.hasEleven = !localFilters.classes.hasEleven;
            this.colorOption(localFilters.classes.hasEleven, btnElem);
            break;
          case 'XII':
            localFilters.classes.hasTwelve = !localFilters.classes.hasTwelve;
            this.colorOption(localFilters.classes.hasTwelve, btnElem);
            break;
          default:
            return;
        }
    
        break;
      
      case 'Subject':
        switch(value) {
          case 'Math':
            localFilters.subjects.hasMath = !localFilters.subjects.hasMath;
            this.colorOption(localFilters.subjects.hasMath, btnElem);
            break;
          case 'Physics':
            localFilters.subjects.hasPhysics = !localFilters.subjects.hasPhysics;
            this.colorOption(localFilters.subjects.hasPhysics, btnElem);
            break;
          case 'Chemistry':
            localFilters.subjects.hasChemistry = !localFilters.subjects.hasChemistry;
            this.colorOption(localFilters.subjects.hasChemistry, btnElem);
            break;
          case 'Biology':
            localFilters.subjects.hasBiology = !localFilters.subjects.hasBiology;
            this.colorOption(localFilters.subjects.hasBiology, btnElem);
            break;
          default:
            return;
        }

        break;
      
      default:
        return;
    }

    // update classes state
    this.setState({
      filters: localFilters
    }, () => {
      // now apply filters....
      this.applyAllFilters();

    });

  }

  applyAllFilters = () => {
     
    // filter out centers data using allcenters as main list
    const localCenters = this.state.allCenters.filter( center => {

      // if filter is true - then compare center data...otherwise...just show it...
      // we don't want to hide data if condition not met
      // example...10 centers, 3 has class 9...others don't
      // when user selects to filter by class 9 - then show only class 9 stuff (first case, comparison)
      // but when user deselects filter - then don't apply any filter (no reason to show NON-class 9 stuff...we filter to shorten only)
      // basic idea...when user deselects..doesn't want any filter...not OPPOSITE condition of NOT having class 9 (nobody Filterses that)

      let hasNine = true;
      let hasTen = true;
      let hasEleven = true;
      let hasTwelve = true;

      let hasMath = true;
      let hasPhysics = true;
      let hasChemistry = true;
      let hasBiology = true;

      // classes
      if(this.state.filters.classes.hasNine) { 
        hasNine = center.data.hasNine === true;
      }
      if(this.state.filters.classes.hasTen) { 
        hasTen = center.data.hasTen === true;
      }
      if(this.state.filters.classes.hasEleven) { 
        hasEleven = center.data.hasEleven === true;
      }
      if(this.state.filters.classes.hasTwelve) { 
        hasTwelve = center.data.hasTwelve === true;
      }

      // subjects
      if(this.state.filters.subjects.hasMath) { 
        hasMath = center.data.hasMath === true;
      }
      if(this.state.filters.subjects.hasPhysics) { 
        hasPhysics = center.data.hasPhysics === true;
      }
      if(this.state.filters.subjects.hasChemistry) { 
        hasChemistry = center.data.hasChemistry === true;
      }
      if(this.state.filters.subjects.hasBiology) { 
        hasBiology = center.data.hasBiology === true;
      }

      return hasNine && hasTen && hasEleven && hasTwelve && 
             hasMath && hasPhysics && hasChemistry && hasBiology;

    });

    console.log(this.state.filters);
    console.log('filtered centers');
    console.log(localCenters);

    this.setState({
      centers: localCenters
    }, () => {
      console.log('all filters applied');
    });
  }

  handleListingClick = (listingData) => {
    console.log('listing clicked');
    console.log(listingData);

    this.handleMapDetailBtnClick(false); // open the detail view

    this.setState({
      selectedCenter: listingData
    });
    
  }

  handleMapDetailBtnClick = (isMapView) => {
    console.log('handleMapDetailBtnClick');
    console.log(isMapView);
    
    this.setState({
      isMapView: isMapView
    });
  }

  onMarkerClick = (selectedCenter) => {
    console.log('Marker Clicked');
    console.log(selectedCenter);
    this.handleListingClick(selectedCenter);
  }

  componentDidMount() {

    // TODO: get current location and make that default on map...for now showing entire delhi/mostly
    // navigator.geolocation.getCurrentPosition(function(position) {
    //   console.log(position); //o_something(position.coords.latitude, position.coords.longitude);
    // });

    // on initial data get
    this.database.on("value", (dataSnapshot) => {
      const data = dataSnapshot.val();
      let list = []; // list of all centers
      
      for(let prop in data) {
        list.push(data[prop]);
      };

      this.setState({
        centers: list,
        allCenters: list
      });
      
    });
  };

  render() {
    return (
      <div className="App">
        
        <Header />
        <AllPanels centers={this.state.centers} 
                  handleListingClick={this.handleListingClick}
                  handleMapDetailBtnClick={this.handleMapDetailBtnClick}
                  isMapView={this.state.isMapView}
                  selectedCenter={this.state.selectedCenter}
                  handleFilterClick={this.handleFilterClick}
                  onMarkerClick={this.onMarkerClick}
                  onResetFilters={this.onResetFilters}
                  filters={this.state.filters}
        />
      </div>
    );
  }
}

export default App;
