import React from 'react';


class Listing extends React.Component {
  
    render() {
  
      console.log('Listing');
      console.log(this.props);
      const name = this.props.name;
      const data = this.props.data;
      let listingStyle = "Listing " + (name === this.props.selectedCenter.name ? "selectedListing": "" ); 
      
      return (
        // on click pass the entire lising object
        <div className={listingStyle} onClick={ () => this.props.handleListingClick(this.props)}> 
            <div className="Listing-picture">
            </div>
          <div className="Listing-info">
            <div className="listing-info-locality">
              {data.locality} · {data.zone}
            </div>
            <div className="listing-info-name">
              {name}
            </div>
            
          </div>
        </div>
      );
    }
}
  
class Listings extends React.Component {
    render() {
  
      console.log('Listings');
      console.log(this.props);
      const centers = this.props.centers;
  
      return (
        <div className="Listings">
          {
            centers.map( (center) => {
              return <Listing key={center.name} selectedCenter={this.props.selectedCenter} name={center.name} data={center.data} handleListingClick={this.props.handleListingClick}/>
            })
          }
          
        </div>
      );
    }
}

export default Listings;