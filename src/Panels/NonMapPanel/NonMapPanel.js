import React from 'react';
import Listings from './Listings'


class FiltersOptions extends React.Component {
    
      render() {
        
        return (
          <div className="Filters-Options">
            {
              this.props.options.map( op => {
                return <div key={op} 
                            className={"option option-" + this.props.type + "-" + op} 
                            onClick={ () => this.props.handleFilterClick(this.props.type, op)}>
                          {op}
                      </div>
              })
            }
          </div>
        );
      }
    }
  
  class Condition extends React.Component {
  
    render() {
      return (
        <div className="Filters-Condition">
          
          <div className="condition-name">
            {this.props.name}
          </div>
          
          <FiltersOptions options={this.props.options} handleFilterClick={this.props.handleFilterClick} type={this.props.name}/>
          
        </div>
      );
    }
  }
  
  class FiltersConditions extends React.Component {
    render() {
      return (
        <div className="Filters-Conditions">
          <Condition name="Class" options={['IX','X', 'XI', 'XII']} handleFilterClick={this.props.handleFilterClick}/>
          <Condition name="Subject" options={['Math', 'Physics', 'Chemistry', 'Biology']} handleFilterClick={this.props.handleFilterClick}/>
          <Condition name="Location" options={['RK Puram','Vasant Kunj', 'Rajouri Garden', 'Dwarka']} handleFilterClick={this.props.handleFilterClick}/>
        </div>
      );
    }
  }
  
  class Filters extends React.Component {
    render() {
      return (
        <div className="Filters"> 
            <FiltersConditions handleFilterClick={this.props.handleFilterClick}/> 
            <div className="FiltersReset" onClick={ () => {this.props.onResetFilters()}}>
              Reset Filters
            </div>
        </div>
      );
    }
  }

class NonMapPanel extends React.Component {
    render() {
  
      console.log('NonMapPanel');
      console.log(this.props);
  
      return (
        <div className="NonMapPanel">
          <Filters filters={this.props.filters} handleFilterClick={this.props.handleFilterClick} onResetFilters={this.props.onResetFilters}/>
          <Listings centers={this.props.centers} handleListingClick={this.props.handleListingClick} selectedCenter={this.props.selectedCenter}/>
        </div>
      );
    }
}


export default NonMapPanel