import React from 'react';

class DetailContent extends React.Component {
    
    render() {
        
        // TODO...if name detail = ..., then show some graphic

        const name = this.props.selectedCenter.name;
        const detail = this.props.selectedCenter.data;
        

        return (
        <div className="DetailContent">
            <div className="DetailName"> {name} </div>
            <div className="DetailSubjectsClasses">
                <div className={ "classTick " +  (detail.hasNine ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> IX </div>
                    <div className="tickIcon"> { detail.hasNine ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
                <div className={ "classTick " +  (detail.hasTen ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> X </div>
                    <div className="tickIcon"> { detail.hasTen ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
                <div className={ "classTick " +  (detail.hasEleven ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> XI </div>
                    <div className="tickIcon"> { detail.hasEleven ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
                <div className={ "classTick " +  (detail.hasTwelve ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> XII </div>
                    <div className="tickIcon"> { detail.hasTwelve ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
            </div>
            <div className="DetailSubjectsClasses">
                <div className={ "classTick " +  (detail.hasMath ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> Maths </div>
                    <div className="tickIcon"> { detail.hasMath ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
                <div className={ "classTick " +  (detail.hasPhysics ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> Physics </div>
                    <div className="tickIcon"> { detail.hasPhysics ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
                <div className={ "classTick " +  (detail.hasChemistry ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> Chemistry </div>
                    <div className="tickIcon"> { detail.hasChemistry ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
                <div className={ "classTick " +  (detail.hasBiology ? "hasClass" : "doesntHaveClass")}>
                    <div className="classLabel"> Biology </div>
                    <div className="tickIcon"> { detail.hasBiology ? <i class="fa fa-check" aria-hidden="true"></i> : <i class="fa fa-times" aria-hidden="true"></i> }</div>
                </div>
            </div>
            
            <div className="DetailContact">
                <div className="ContactBox">
                    <div className="ContactIcon">
                        <i className="fa fa-map-marker" aria-hidden="true"></i>
                    </div> 
                    <div className="ContactText">
                        {detail.address} 
                    </div>
                </div>
                <div className="ContactBox">
                    <div className="ContactIcon">
                        <i className="fa fa-globe" aria-hidden="true"></i>
                    </div> 
                    <div className="ContactText">
                        {detail.website} 
                    </div>
                </div>
                <div className="ContactBox">
                    <div className="ContactIcon">
                        <i className="fa fa-phone" aria-hidden="true"></i>
                    </div> 
                    <div className="ContactText">
                        {detail.phone.join(' , ')} 
                    </div>
                </div>
            </div>
            
            <div className="DetailTiming">
            <div className="ContactBox">
                    <div className="ContactIcon">
                        <i className="fa fa-clock-o" aria-hidden="true"></i>
                    </div> 
                    <div className="daysTime">
                        
                        <div className="dayBox">
                            <div className="dayLabel">
                                Monday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Monday.open} - {detail.timings.Monday.close}
                            </div>
                        </div>
                        <div className="dayBox">
                            <div className="dayLabel">
                                Tuesday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Tuesday.open} - {detail.timings.Tuesday.close}
                            </div>
                        </div>
                        <div className="dayBox">
                            <div className="dayLabel">
                                Wednesday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Wednesday.open} - {detail.timings.Wednesday.close}
                            </div>
                        </div>
                        <div className="dayBox">
                            <div className="dayLabel">
                                Thursday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Thursday.open} - {detail.timings.Thursday.close}
                            </div>
                        </div>
                        <div className="dayBox">
                            <div className="dayLabel">
                                Friday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Friday.open} - {detail.timings.Friday.close}
                            </div>
                        </div>
                        <div className="dayBox">
                            <div className="dayLabel">
                                Saturday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Saturday.open} - {detail.timings.Saturday.close}
                            </div>
                        </div>
                        <div className="dayBox">
                            <div className="dayLabel">
                                Sunday
                            </div>
                            <div className="dayTime">
                                {detail.timings.Sunday.open} - {detail.timings.Sunday.close}
                            </div>
                        </div>
                    
                    </div>
                </div>
            </div>

            
        </div>
        );
    }
}

export default DetailContent;