import React from 'react';
import DetailView from './DetailView'
import MapView from '../../Map/MapView';

class DetailOrMap extends React.Component {
    render() {
  
      console.log('DetailOrMap');
      console.log(this.props);
      
      return (
        <div className="DetailOrMap">
          { 
            // without this map does not render correctly for whichever was hidden earlier...changing display none to show messes up till browser resize
            this.props.isMapView ? <MapView onMarkerClick={this.props.onMarkerClick} selectedCenter={this.props.selectedCenter} centers={this.props.centers} isMapView={this.props.isMapView} zoom={12}/>
                                 : <DetailView selectedCenter={this.props.selectedCenter} isMapView={this.props.isMapView}/>
          }
          
          
        </div>
      );
    }
  }

class MapPanelViewSelectionBtns extends React.Component {
    render() {
  
      console.log('MapPanelViewSelectionBtns');
      console.log(this.props);
      
      const MapBtnClass = "MapBtn " + (this.props.isMapView ? "MapOrDetailBtnSelected" : "" );
      const DetailBtnClass = "DetailBtn " + (this.props.isMapView ? "": "MapOrDetailBtnSelected"); 

      return (
        <div className="MapPanelViewSelectionBtns"> 
            <div className={MapBtnClass} onClick={ () => this.props.handleMapDetailBtnClick(true) }>
              Map
            </div>
              
            <div className={DetailBtnClass} onClick={ () => this.props.handleMapDetailBtnClick(false)}>
              Detail
            </div>
            
          </div>
      );
    }
}



class MapPanel extends React.Component {
    
    render() {
      return (
        <div className="Map">
  
            <MapPanelViewSelectionBtns  isMapView={this.props.isMapView}
                                        handleMapDetailBtnClick={this.props.handleMapDetailBtnClick}
            />
  
          <DetailOrMap  isMapView={this.props.isMapView}
                        selectedCenter={this.props.selectedCenter}
                        centers={this.props.centers}
                        onMarkerClick={this.props.onMarkerClick}
          />
          
          
        </div>
      );
    }
  }


  export default MapPanel;