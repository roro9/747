import React from 'react';
import MapView from '../../Map/MapView';
import DetailContent from './DetailContent'



class DetailPicMap extends React.Component {
    render() {
  
      console.log('DetailpicMap');
      console.log(this.props);
  
  
      return (
        <div className="Detail-picMap">
          <div className="DetailPicture">
            Picture
          </div>
          <div className="DetailMap"> 
          <MapView selectedCenter={this.props.selectedCenter} isFromDetail={true} zoom={17}/>
          </div>
        </div>
      );
    }
}

class DetailView extends React.Component {
    
    render() {
    console.log('DetailView');
    console.log(this.props);
    const style = "Details-view "; // + String(this.props.isMapView ? "displayNone" : "");

    return (
        <div className={style}> 
        <DetailPicMap selectedCenter={this.props.selectedCenter}/>
        <DetailContent selectedCenter={this.props.selectedCenter}/>
        </div>
    );
    }
}

export default DetailView;