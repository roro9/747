import React from 'react';
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    // Marker,
  } from "react-google-maps";
  import { compose, withProps } from "recompose"
  import { MarkerWithLabel } from "react-google-maps/lib/components/addons/MarkerWithLabel";
  const { InfoBox } = require("react-google-maps/lib/components/addons/InfoBox");
  
const MyMapComponent = compose(
withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAcZJCKFBCa2QJaMAKHA97Q792vb91tphg&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div className="Map-loading-elem"/>,
    containerElement: <div className="Map-container-elem"/>,
    mapElement: <div className="Map-mapElement"/>
}),
withScriptjs,
withGoogleMap
)( (props) => 

    <GoogleMap
    defaultZoom={props.zoom}
    center={props.selectedCenter.data.coord}
    
    >
    {
    // if has list of all centers....then show all markers  (in pure main map view mode)
    // centers={this.props.centers}
    props.centers ? props.centers.map( center => {
        return (
        <MarkerWithLabel 
          key={center.name} 
          position={center.data.coord}
          className="MapMarker"
          labelStyle={ {
                          backgroundColor: "#F42730", 
                          fontSize: "1em", 
                          padding: "1em",
                          paddingTop: "0.5em",
                          paddingBottom: "0.5em",
                          color: "white",
                          opacity: "0.9",
                          borderRadius: "5px"

                        }
                    }
          labelAnchor={center.data.coord}
          onClick={ () => props.onMarkerClick(center) }
        >
        <div>{center.name}</div>
        </MarkerWithLabel>
        )
    }) 

    :
    
    <MarkerWithLabel 
      position={props.selectedCenter.data.coord} 
      labelStyle={ {
                          backgroundColor: "#F42730", 
                          fontSize: "1em", 
                          padding: "1em",
                          paddingTop: "0.5em",
                          paddingBottom: "0.5em",
                          color: "white",
                          opacity: "0.9",
                          borderRadius: "5px"

                        }
                    }
      labelAnchor={props.selectedCenter.data.coord}
    >
    <div>{props.selectedCenter.name}</div>
    </MarkerWithLabel>
    
    }
    
    </GoogleMap>
)

class MapView extends React.Component {
    render() {
      console.log('MapView');
      console.log(this.props);
      
      // when in detail view
      // if detail view map...don't hide map, otherwise if main map..hide
      const style = this.props.isFromDetail; // ? "" : String(this.props.isMapView ? "" : " displayNone");
  
      return (
        
        <div className={"MainMapViewDiv " + style}>
          <MyMapComponent selectedCenter={this.props.selectedCenter} 
                          zoom={this.props.zoom} 
                          centers={this.props.centers} 
                          onMarkerClick={this.props.onMarkerClick}/>
        </div>
        
  
        
      );
    }
  }

  export default MapView;