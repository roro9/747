export const DB_CONFIG = {
    apiKey: "AIzaSyB_df0csxzEiq1mFAN9p-pzjbUx0rmMTDE",

    // Only needed if using Firebase Realtime Database (which we will be in this example)
    databaseURL: "https://s747-1510024662422.firebaseio.com",

    // Only needed if using Firebase Authentication
    authDomain: "s747-1510024662422.firebaseapp.com",

    // Only needed if using Firebase Storage
    storageBucket: "s747-1510024662422.appspot.com"
};

  
